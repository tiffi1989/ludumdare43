﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Brick : MonoBehaviour {
  float counter;
 public bool grounded;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

  }

  public bool IsBlockGrounded()
  {
    return grounded;
  }

  private void OnCollisionEnter2D(Collision2D collision)
  {
    grounded = true;
  }


}
