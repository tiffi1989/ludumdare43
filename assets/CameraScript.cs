﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour
{

  Vector3 startPosition;
  public float speed;
  private float counter;
  private Vector3 target;

  // Use this for initialization
  void Start()
  {
    startPosition = new Vector3(transform.position.x, transform.position.y, transform.position.z);
    target = startPosition;
  }

  // Update is called once per frame
  void Update()
  {
    counter++;
    if (counter > 20)
    {
      counter = 0;
      GameObject[] allBricks = GameObject.FindGameObjectsWithTag("brick");
      float highestBrick = 0;

      foreach (GameObject brick in allBricks)
      {
        if (!brick.GetComponent<Collider2D>().isTrigger && brick.transform.position.y > highestBrick && brick.GetComponent<Brick>().IsBlockGrounded())
          highestBrick = brick.transform.position.y;
      }
      if (highestBrick > 7)
        target = new Vector3(0, highestBrick, -10);

    }


    float step = speed * Time.deltaTime;
    transform.position = Vector3.MoveTowards(transform.position, target, step);
  }
}
