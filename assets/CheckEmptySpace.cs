﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckEmptySpace : MonoBehaviour {

  SpriteRenderer renderer;
  bool spaceEmpty = true;
  bool blink;
  public bool isFixed;

	// Use this for initialization
	void Start () {
    renderer = GetComponent<SpriteRenderer>();
	}
	
	// Update is called once per frame
	void Update () {
    /*if (blink)
    {
      if (renderer.color.g > 0.1f)
        renderer.color = new Color(renderer.color.r, renderer.color.g - 2f * Time.deltaTime, renderer.color.b);
      else
        blink = false;
    } else
    {
      if (renderer.color.g < 0.98f)
        renderer.color = new Color(renderer.color.r, renderer.color.g + 2f * Time.deltaTime, renderer.color.b);
    }*/

    if(spaceEmpty)
    {
      renderer.material.color = Color.white;
    } else
    {
      renderer.material.color = Color.red;
    }

  }


  public bool CheckSpace()
  {
    if(!spaceEmpty)
    {
      blink = true;
    }
    return spaceEmpty;
  }

  private void OnTriggerStay2D(Collider2D collision)
  {
    if (collision.gameObject.tag == gameObject.tag)
      spaceEmpty = false;
    if (collision.gameObject.tag == "light" && isFixed && GetComponent<Rigidbody2D>().gravityScale == 0)
      collision.gameObject.GetComponent<Light>().intensity = 0;
  }

  private void OnTriggerExit2D(Collider2D collision)
  {
    if (collision.gameObject.tag == gameObject.tag)
      spaceEmpty = true;
  }

  private void OnCollisionExit2D(Collision2D collision)
  {
    if (collision.gameObject.tag == gameObject.tag)
      spaceEmpty = true;
  }


}
