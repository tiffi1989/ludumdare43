﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Countdown : MonoBehaviour
{

  private PlayerControls playerControls;
  private Text text;
  private Tutorial tutorial;
  private bool hurryTextSent;
  // Use this for initialization
  void Start()
  {
    playerControls = GameObject.Find("ScriptHolder").GetComponent<PlayerControls>();
    text = GetComponent<Text>();
    tutorial = GameObject.Find("Tutorial").GetComponent<Tutorial>();
  }

  // Update is called once per frame
  void Update()
  {
    text.text = (((int)(playerControls.countdown * 100)) / 100f).ToString();
    if (playerControls.countdown < 5)
    {
      if (!hurryTextSent)
      {
        tutorial.SetText("Hurry!");
        hurryTextSent = true;
      }
      text.color = Color.red;
      text.fontSize = 45;
    }
    else
    {
      text.color = new Color(0, 0, 0, 0);
      hurryTextSent = false;
      text.fontSize = 30;
    }


  }
}
