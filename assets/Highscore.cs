﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Highscore : MonoBehaviour
{

  Text text;
  int counter;
  public int highscore;
  PlayerControls player;
  private float lastHighest;

  // Use this for initialization
  void Start()
  {
    text = GetComponent<Text>();
    player = GameObject.Find("ScriptHolder").GetComponent<PlayerControls>();
  }

  // Update is called once per frame
  void Update()
  {
    counter++;
    if (counter > 20)
    {
      counter = 0;
      GameObject[] allBricks = GameObject.FindGameObjectsWithTag("brick");
      float highestBrick = 0;

      foreach (GameObject brick in allBricks)
      {
        if (!brick.GetComponent<Collider2D>().isTrigger && brick.transform.position.y > highestBrick && brick.GetComponent<Brick>().IsBlockGrounded())
        {
          highestBrick = brick.transform.position.y;

          if (highestBrick >= lastHighest)
          {
            lastHighest = highestBrick;
          }
        }
        highscore = (int)lastHighest * (10);

      }
      int levelBonus = player.Level * 1000;
      text.text = "Highscore: " + (highscore+ levelBonus) + "\n\nCurrent Height: " + (int)(highestBrick );

    }
  }
}
