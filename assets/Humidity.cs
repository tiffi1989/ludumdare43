﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Humidity : MonoBehaviour {

  private float timeNeeded = 30;
  private float time;
  private Text text;
	// Use this for initialization
	void Start () {
    text = GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
    time += Time.deltaTime;
    if(time > timeNeeded)
    {
      text.text = "Humidity: " + Random.Range(0, 100) + " %";
      time = 0;
      timeNeeded = Random.Range(10, 100);
    }
	}
}
