﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightScript : MonoBehaviour {
  Light pointLight;
	// Use this for initialization
	void Start () {
    pointLight = GetComponent<Light>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

  private void OnTriggerStay2D(Collider2D collision)
  {
    Debug.Log("enter");
    if (collision.gameObject.tag == "cut")
      pointLight.enabled = false;
    if (collision.gameObject.tag == "steel")
    {
      pointLight.intensity = 1;
      pointLight.enabled = true;
    }
  }

  private void OnTriggerExit2D(Collider2D collision)
  {
    Debug.Log("enter");

    if (collision.gameObject.tag == "cut")
      pointLight.enabled = true;
  }

  private void OnTriggerEnter2D(Collider2D collision)
  {
    Debug.Log("collllll" + collision.gameObject.tag);
    if (collision.gameObject.tag == "steel")
    {
      pointLight.intensity = 1;
      pointLight.enabled = true;
    }

  }
}
