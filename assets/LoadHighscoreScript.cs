﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class LoadHighscoreScript : MonoBehaviour
{

  public Text highscores;

  // Use this for initialization
  void Start()
  {

  }

  // Update is called once per frame
  void Update()
  {

  }

  public void WriteHighscore(string name, int score)
  {
    StartCoroutine(SetHighscore(name, score));
    GetComponent<Image>().enabled = true;
    ;
    foreach(Text text in GetComponentsInChildren<Text>())
    {
      text.enabled = true;
    }


  }

  IEnumerator SetHighscore(string name, int score)
  {
    UnityWebRequest www = UnityWebRequest.Get("http://138.68.85.198:1337/setHighscore?name=" + name + "&score="+ score);
    yield return www.SendWebRequest();

    if (www.isNetworkError || www.isHttpError)
    {
      Debug.Log(www.error);
    }
    else
    {
      StartCoroutine(GetHighscore());
    }
  }

  IEnumerator GetHighscore()
  {
    UnityWebRequest www = UnityWebRequest.Get("http://138.68.85.198:1337/getAllHighscores");
    yield return www.SendWebRequest();

    if (www.isNetworkError || www.isHttpError)
    {
      Debug.Log(www.error);
    }
    else
    {
      highscores.text = www.downloadHandler.text;

      ;
    }
  }
}
