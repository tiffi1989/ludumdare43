﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Name : MonoBehaviour {
  private Tutorial tutorial;
  private PlayerControls player;
  public Text textField;
	// Use this for initialization
	void Start () {
    tutorial = GameObject.Find("Tutorial").GetComponent<Tutorial>();
    player = GameObject.Find("ScriptHolder").GetComponent<PlayerControls>();
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.Return))
    {
      string name = textField.text;
      if(name.Length < 2 || name.Length > 20)
      {
        tutorial.SetText("Stop fucking around, enter your Name");
      } else
      {
        player.name = name;
        gameObject.SetActive(false);
      }
    }
	}
}
