﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Next : MonoBehaviour
{
  public Sprite[] sprites;
  private int counter;
  PlayerControls player;
  UnityEngine.UI.Image image;
  SpriteRenderer renderer;
  // Use this for initialization
  void Start()
  {
    image = GetComponent<Image>();
    player = GameObject.Find("ScriptHolder").GetComponent<PlayerControls>();
  }

  // Update is called once per frame
  void Update()
  {
    counter++;
    if (counter > 20)
    {
      image.sprite = sprites[player.nextForm];
      counter = 0;
    }
  }
}
