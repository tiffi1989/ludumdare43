﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class PlayerControls : MonoBehaviour
{

  enum State { enterName, selectForm, cutFromScreen, placeNewBrick, idle, start, end }

  private State currentState = State.enterName;
  private static int amountOfPrefabs = 4;
  private GameObject[] brickPrefabs = new GameObject[amountOfPrefabs];
  private GameObject[] cutFormPrefabs = new GameObject[amountOfPrefabs];

  public string name;

  private GameObject currentForm;
  private GameObject currentBrick;
  public Highscore highscore;

  private float countDownLength = 50;
  public float countdown = 50;

  private int currentSelectedPrefab;
  public int nextForm;
  private Tutorial tutorial;
  public int Level = 0;
  private float scale = .5f;

  private bool firstTimeCut, firstTimeBrick, firstTimeSteel, firstTimeTri, firstTimeBrickSet;

  // Use this for initialization
  void Start()
  {
    tutorial = GameObject.Find("Tutorial").GetComponent<Tutorial>();
    for (int i = 0; i < amountOfPrefabs; i++)
    {
      brickPrefabs[i] = (GameObject)Resources.Load("Prefabs/brick" + (i + 1), typeof(GameObject));
      cutFormPrefabs[i] = (GameObject)Resources.Load("Prefabs/form" + (i + 1), typeof(GameObject));

    }
  }


  void Controls()
  {


    if (Input.GetMouseButtonDown(0))
    {
      switch (currentState)
      {
        case State.cutFromScreen:
          if (currentForm.GetComponent<CheckEmptySpace>().CheckSpace())
          {
            GameObject form = Instantiate(cutFormPrefabs[currentSelectedPrefab], currentForm.transform.position, currentForm.transform.rotation);
            form.transform.localScale = new Vector3(scale, scale, 1);
            currentBrick = Instantiate(brickPrefabs[currentSelectedPrefab], currentForm.transform.position, currentForm.transform.rotation);
            form.transform.SetParent(Camera.main.transform);
            form.GetComponent<CheckEmptySpace>().isFixed = true;
            GameObject.Destroy(currentForm);
            currentForm = null;
            currentState = State.placeNewBrick;
            countdown = countDownLength;
            if (!firstTimeBrick)
            {
              tutorial.SetText("Ok, we lost some sight, but who cares? \nAll I care about is building TOWERS!! Click to place that brick!", 5);
              firstTimeBrick = true;
            }

          }
          break;
        case State.placeNewBrick:
          if (currentBrick.GetComponent<CheckEmptySpace>().CheckSpace())
          {
            currentBrick.GetComponent<Collider2D>().isTrigger = false;
            currentBrick.GetComponent<Rigidbody2D>().velocity = new Vector2();
            currentBrick.GetComponent<Rigidbody2D>().isKinematic = false;
            currentBrick = null;
            countdown = countDownLength;
            CreateNewForm();
            if (!firstTimeBrickSet)
            {
              tutorial.SetText("Good work. Every monkey could have done that, but still GREAT WORK!", 3);
              firstTimeBrickSet = true;
              Invoke("ActivateGui", 6);
            }

          }

          break;
        default: break;
      }
    }
    


    if (Input.GetAxis("Horizontal") != 0)
    {
      GameObject objectToRotate = null;
      switch (currentState)
      {
        case State.cutFromScreen:
          objectToRotate = currentForm;
          break;
        case State.placeNewBrick:
          objectToRotate = currentBrick;
          break;
        default: break;
      }
      if (objectToRotate)
      {
        objectToRotate.transform.Rotate(Vector3.back, Input.GetAxis("Horizontal") * Time.deltaTime * 150);
        // Quaternion.AngleAxis(90, new Vector3(0, 0, 1));
        // objectToRotate.transform.rotation = new Quaternion(objectToRotate.transform.rotation.x, objectToRotate.transform.rotation.y, objectToRotate.transform.rotation.z + , objectToRotate.transform.rotation.w);
      }
    }
  }

  public void ActivateGui()
  {
    GameObject.Find("GUI").GetComponent<Canvas>().enabled = true;
  }
  public void DeactivateGui()
  {
    GameObject.Find("GUI").GetComponent<Canvas>().enabled = false;
  }

  void PositionItemOnMouse()
  {
    switch (currentState)
    {
      case State.cutFromScreen:
        if (currentForm)
        {
          Vector3 pos = Input.mousePosition;
          pos.z = currentForm.transform.position.z - Camera.main.transform.position.z;
          currentForm.transform.position = Camera.main.ScreenToWorldPoint(pos);
        }
        break;
      case State.placeNewBrick:
        if (currentBrick)
        {
          Vector3 pos = Input.mousePosition;
          pos.z = currentBrick.transform.position.z - Camera.main.transform.position.z;

          currentBrick.transform.position = Camera.main.ScreenToWorldPoint(pos);
        }
        break;
      default: break;
    }
  }

  void CreateNewForm()
  {
    CreateForms(nextForm);
    switch (Level)
    {
      case 0:
        nextForm = Random.Range(0, 1);
        scale = .5f;
        break;
      case 1:
        nextForm = Random.Range(0, 2);
        countDownLength = 10;
        countdown = 10;
        scale = .5f;

        break;
      case 2:
        nextForm = Random.Range(0, 3);
        countDownLength = 7;
        scale = .5f;

        break;
      case 3:
        nextForm = Random.Range(0, 4);
        countDownLength = 5;
        scale = .7f;

        break;
      default:
        nextForm = Random.Range(0, 4);
        scale = 1f;

        break;

    }

  }

  void CreateForms(int form)
  {
    currentForm = Instantiate(cutFormPrefabs[form], new Vector3(-1000, 0, 0), Quaternion.identity);
    currentForm.transform.localScale = new Vector3(scale, scale, 1);
    currentState = State.cutFromScreen;
    currentSelectedPrefab = form;

  }

  // Update is called once per frame
  void Update()
  {


    if (!firstTimeCut && currentState == State.cutFromScreen)
    {
      firstTimeCut = true;
      tutorial.SetText("To get new tower building materials, we have to cut some stuff from our screen. Cut by using left Mouse button", 5);
    }

    if (!firstTimeSteel && Camera.main.transform.position.y > 15)
    {
      tutorial.SetText("Look! A steel beam, your tower looks like crap, but if we manage to get there the tower will be stabilized.", 5);
      firstTimeSteel = true;
      Invoke("ActivateGui", 6);
      DeactivateGui();

    }


    countdown -= Time.deltaTime;
    switch (currentState)
    {
      case State.enterName:
        if (name.Length > 1)
        {
          currentState = State.start;
        }
        break;
      case State.start: CreateNewForm(); break;
      case State.idle: break;

      case State.cutFromScreen:
        break;
      case State.placeNewBrick:
        break;
      case State.end:
        if (Input.GetKeyDown(KeyCode.R))
        {
          SceneManager.LoadScene("MainScene");
        }
        if (Input.GetKeyDown(KeyCode.Escape))
        {
          Application.Quit();
        }
        break;
      default: break;
    }
    Controls();


    if (countdown < 0.0f)
    {
      countdown = 0;
      if (currentState != State.end)
      {
        GameObject.Find("HighscorePanel").GetComponent<LoadHighscoreScript>().WriteHighscore(name, highscore.highscore + Level * 1000);
      }
      currentState = State.end;
    }
    else
    {

    }
    PositionItemOnMouse();

  }
}
