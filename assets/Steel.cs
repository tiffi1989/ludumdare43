﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Steel : MonoBehaviour
{
  bool solid;

  // Use this for initialization
  void Start()
  {

  }

  // Update is called once per frame
  void Update()
  {
    if(Camera.main.transform.position.y > transform.position.y && !solid)
    {
      solid = true;
      GetComponent<SpriteRenderer>().color = Color.white;
      GetComponent<Collider2D>().isTrigger = false;
      gameObject.tag = "solidSteel";
      GameObject.Find("ScriptHolder").GetComponent<PlayerControls>().Level += 1 ;
    }
  }

  private void OnTriggerEnter2D(Collider2D collision)
  {
    if (collision.gameObject.tag == "cut" && gameObject.tag != "solidSteel")
    {
      collision.gameObject.GetComponent<Rigidbody2D>().gravityScale = 1;

    }
  }
}
