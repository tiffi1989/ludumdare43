﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tutorial : MonoBehaviour
{
  enum State { showText, blurText, hideText }
  Text text;
  State currentState = State.hideText;
  float timer;
  int duration = 4;
  // Use this for initialization
  void Start()
  {
    text = GetComponent<Text>();
  }

  // Update is called once per frame
  void Update()
  {
    timer += Time.deltaTime;
    switch (currentState)
    {
      case State.showText:
        if (timer > duration) currentState = State.blurText;
        break;
      case State.blurText:
        text.color = new Color(text.color.r, text.color.g, text.color.b, text.color.a - 0.2f * Time.deltaTime);
        if (text.color.a < 0.1f)
          currentState = State.hideText;
        break;
      case State.hideText:
        text.text = "";
        break;
      default: break;
    }
  }

  public void SetText(string text)
  {
    SetText(text, 4);
  }

  public void SetText(string text, int duration)
  {
    this.duration = duration;
    this.text.text = text;
    currentState = State.showText;
    this.text.color = Color.black;
    timer = 0;

  }
}
